**This Repo hast moved to GitHub and will no longer be updated.**
**New Repo: [wielebenwir/commons-booking on Github](https://github.com/wielebenwir/commons-booking) 
**







# Commons Booking
![Logo](./commons-booking/assets/cb-logo.png)

Wordpress plugin for managing and booking of common goods. 

[Florian Egermann](http://www.fleg.de) / [wielebenwir e.V](http://www.wielebenwir.de). 

This plugin gives associations, groups and individuals the ability to share items (e.g. cargobikes, tools) with users. It is based on the idea of Commons and sharing resources for the benefit of the community. 

It was developed for the "free cargo bike" movement in Germany and Austria, but it can use it for any kind items. About the movement:  http://www.wielebenwir.de/mobilitaet/free-nomadic-cargo-bikes-are-changing-the-urban-landscape

# Download

Hosted here are development versions, if you don´t want to participate in beta-testing, please download stable releases form [Wordpress.org](https://wordpress.org/plugins/commons-booking/)

# Unique features:

* Items, locations and timeframes: No need for a "centralised storage", items can be assigned to different locations for the duration of a timeframe, each with their own contact information.  
* Simple booking process: A booking is at least one full day, just pick the date on the calendar.
* Auto-accept bookings: A registered user can book items without the need for administration. 
* Codes: The plugin automatically generates booking codes, which are used at the station to validate the booking. 
* Users can leave booking comments (e.g. what they use the item for).


## Possible use cases:

* You have some special tools that you don´t use every day, and you want to make them available to your friends.
*  You own a cargo bike that you want to share with the community, and it will be placed at different locations throughout the year.

## Plugin websites

* [Official Website (German)](http://www.wielebenwir.de/projekte/commons-booking)
* [Official WIKI (German)](http://dein-lastenrad.de/index.php?title=Commons_Booking_Software)
* [Bug-Tracker](https://bitbucket.org/wielebenwir/commons-booking/issues?status=new&status=open ) 
* [Bulletin Board (German)](http://forum.dein-lastenrad.de/index.php?p=/categories/buchungs-software)

## Credits

### Development Support

* Christian Wenzel ([wielebenwir e.V.](http://www.wielebenwir.de) /
    [KASIMIR – Dein Lastenrad](http://kasimir-lastenrad.de))

### Beta-Testing

* Ralph W ([Klara – Das kostenlose Lastenrad für
    Hamburg](https://klara.bike))
* Sven Baier ([Hannah – Lastenräder für
    Hannover](http://www.hannah-lastenrad.de))
* Albert Hölzle ([VeloCityRuhr](http://velocityruhr.net))
* Stefan Meretz ([Bolle – Dein Freier Lastenanhänger für´s
    Fahrrad](http://bolle-bonn.de))
* Hannes Wöhrle ([wielebenwir e.V.](http://www.wielebenwir.de) /
    [KASIMIR – Dein Lastenrad](http://kasimir-lastenrad.de))

### Translations

* Albert Hölzle ([VeloCityRuhr](http://velocityruhr.net))
* Stefan Meretz ([Bolle – Dein Freier Lastenanhänger für´s Fahrrad](http://bolle-bonn.de))  
*  Sven Baier ([Hannah – Lastenräder für Hannover](http://www.hannah-lastenrad.de))


### Built using:

* Built with [The WordPress Plugin Boilerplate Powered ](https://github.com/sudar/wp-plugin-in-github/wiki) 
*  [CMB2](https://github.com/WebDevStudios/Custom-Metaboxes-and-Fields-for-WordPress)
*  [CPT_Core](https://github.com/WebDevStudios/CPT_Core)
*  [Taxonomy_Core ]( https://github.com/WebDevStudios/Taxonomy_Core )
   

##License

> This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

> This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.